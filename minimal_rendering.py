# -*- coding: utf-8 -*-

import math
import random
import numpy as np
import pyglet
from pyglet.gl import (
    Config,
    glEnable, glBlendFunc, glLoadIdentity, glClearColor,
    GL_BLEND, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA,
    glPushMatrix, glPopMatrix, glBegin, glEnd, glColor3f,
    glVertex2f, glTranslatef, glRotatef,
    GL_LINE_LOOP, GL_LINES, GL_TRIANGLES, GL_QUADS)

BOID_RANGE = 50.0
BOID_VIEW_ANGLE = 135


class Boid:
    def __init__(self,
                 position=[100.0, 100.0],
                 velocity=[0.0, 0.0],
                 size=10.0,
                 color=[1.0, 1.0, 1.0]):
        self.position = np.array(position)
        self.velocity = np.array(velocity)
        self.size = size
        self.color = color

    def __repr__(self):
        return f"Boid: position={self.position}, velocity={self.velocity}, color={self.color}"

    def render_velocity(self):
        glColor3f(0.6, 0.6, 0.6)
        glBegin(GL_LINES)
        glVertex2f(0.0, 0.0)
        glVertex2f(0.0, BOID_RANGE)
        glEnd()

    def render_view(self):
        glColor3f(0.6, 0.6, 0.1)
        glBegin(GL_LINE_LOOP)

        step = 30
        for i in range(-BOID_VIEW_ANGLE, BOID_VIEW_ANGLE + step, step):
            glVertex2f(BOID_RANGE * math.sin(math.radians(i)),
                       (BOID_RANGE * math.cos(math.radians(i))))
        glVertex2f(0.0, 0.0)
        glEnd()

    def render_boid(self):
        glBegin(GL_TRIANGLES)
        glColor3f(*self.color)
        glVertex2f(-self.size, 0.0)
        glVertex2f(self.size, 0.0)
        glVertex2f(0.0, self.size * 3.0)
        glEnd()

    def draw(self):
        glPushMatrix()
        glTranslatef(self.position[0], self.position[1], 0.0)
        glRotatef(math.degrees(math.atan2(self.velocity[0], self.velocity[1])), 0.0, 0.0, -1.0)
        self.render_velocity()
        self.render_view()
        self.render_boid()
        glPopMatrix()


def get_window_config():
    display = pyglet.canvas.get_display()
    screen = display.get_default_screen()

    template = Config(double_buffer=True, sample_buffers=1, samples=4)
    try:
        config = screen.get_best_config(template)
    except pyglet.window.NoSuchConfigException:
        template = Config()
        config = screen.get_best_config(template)
    return config


if __name__ == "__main__":
    window = pyglet.window.Window(
        fullscreen=False,
        caption="Boid rendering",
        config=get_window_config())

    current_cell = 0

    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    boid = Boid(
        position=[np.random.uniform(0, window.width), np.random.uniform(0, window.height)],
        velocity=200 * np.random.uniform(-1, 1, 2),
        color=[random.random(), random.random(), random.random()])

    fps_display = pyglet.window.FPSDisplay(window=window)


    def update(dt):
        boid.position += boid.velocity * dt
        theta = np.radians(np.random.normal(0, 5))
        c, s = np.cos(theta), np.sin(theta)
        R = np.array(((c, -s), (s, c)))
        boid.velocity = R.dot(boid.velocity)
        boid.position %= np.array([window.width, window.height])


    def draw_checkerboard():
        glBegin(GL_QUADS)
        grid_size = 20

        global current_cell

        cell_width = window.width / grid_size
        cell_height = window.height / grid_size

        for x in range(grid_size):
            for y in range(grid_size):
                if (x * grid_size + y) % (grid_size * grid_size) == int(current_cell):
                    color = [0, 0, 1]
                else:
                    s = (x + y) % 2
                    color = [s, s, s]
                glColor3f(*color)

                # Calculer les positions des coins du quad
                x1 = x * cell_width
                y1 = y * cell_height
                x2 = x1 + cell_width
                y2 = y1 + cell_height

                # Dessiner le quad
                glVertex2f(x1, y1)
                glVertex2f(x2, y1)
                glVertex2f(x2, y2)
                glVertex2f(x1, y2)
        glEnd()
        current_cell += 0.1


    @window.event
    def on_draw():
        glClearColor(0.1, 0.1, 0.1, 1.0)
        window.clear()
        glLoadIdentity()
        draw_checkerboard()
        boid.draw()
        fps_display.draw()


    # schedule world updates as often as possible
    pyglet.clock.schedule(update)
    pyglet.app.run()
