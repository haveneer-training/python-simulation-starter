# python-simulation-starter

* initialize sandbox
```shell
docker run -it -v $PWD:/data --platform linux/amd64 python:3.12 /bin/bash 
```

* download all packages
```shell
pip3 download -r requirements.txt
```

* create virtual environment
```shell
python3.12 -m venv virtual_env && source ./virtual_env/bin/activate 
```

* install from previously downloaded packages (offline)

```shell
pip3 install --no-index --find-links $PWD/offline_packages -r requirements.txt
```